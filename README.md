# Ikea desk scraper

# Deployment

First we need to create the dependency folder (`ikea-desk-scraper/package`):
> `pip install --target ./package twilio requests bs4`

If `app.zip` already exists, run:
>`rm -rf app.zip`

From package directory (ikea-desk-scraper/package) and run:
>`zip -r ../app.zip .`

This will create `app.zip` archive in the project root.

Add the code to the zip archive - run (from ikea-desk-scraper/app):
>`zip -g ../app.zip lambda_function.py`

This will produce the following output
>`adding: app/lambda_function.py (deflated 56%)`

Go to AWS lambda > Code tab > Code Source > Upload from > select the zip - that's it.

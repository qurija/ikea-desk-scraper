import requests
from twilio.rest import Client
from bs4 import BeautifulSoup
import os


DESKS = {
    "idasen_frame_only": "https://www.ikea.com/rs/sr/p/idasen-podokvir-ploce-podesiv-stola-elek-tamnosiva-00320723/",
    "idasen_wood_cream": "https://www.ikea.com/rs/sr/p/idasen-podesivi-pisaci-sto-smeda-bez-s19280915/",
    "idasen_black_black": "https://www.ikea.com/rs/sr/p/idasen-podesivi-pisaci-sto-crna-tamnosiva-s19280939/",
    "idasen_wood_black": "https://www.ikea.com/rs/sr/p/idasen-podesivi-pisaci-sto-smeda-tamnosiva-s79280955/",
}

def main(event, context):
    client = Client(os.environ.get('TWILIO_ACCOUNT_SID'), os.environ.get('TWILIO_AUTH_TOKEN'))

    results = {}
    for desk, url in DESKS.items():
        results[desk] = get_price(url)

    body = "\n".join(f"{k} {v}" for k, v in results.items())
    message = client.messages \
                    .create(
                        body=body,
                        from_=os.environ.get('TWILIO_PHONE_NUMBER'),
                        to=os.environ.get('MY_PHONE_NUMBER')
                    )


def get_price(url):
    request = requests.get(url)
    if not request.ok:
        return  # handle error
    soup = BeautifulSoup(request.text, 'html.parser')
    div = soup.find("div", {"class": "range-revamp-product__buy-module-container range-revamp-product__grid-gap"})
    price = div.find("span", {"class": "range-revamp-price__integer"}).text

    return price
